/* globals lang, initRequire*/
include('i18n/i18n.js');
include('libs/utils/JSON.prune.js');
include("libs/Oracle/smartface.mcs.js");
Application.onUnhandledError = Application_OnError;
Application.onStart = Application_OnStart;
include("libs/Smartface/require.js");
include('libs/utils/smartface.tiny.utils.js');

var smfOracle;

/**
 * Triggered when application is started.
 * @param {EventArguments} e Returns some attributes about the specified functions
 * @this Application
 */
function Application_OnStart(e) {
	SMF.UI.statusBar.visible = true;
	
	// Creating a new Oracle MCS instance 
	smfOracle = new SMF.Oracle.MobileCloudService('smartfaceOracleMCS');
	
	// logging in as anonymous user to log Analytics events
	// if you need you can auth user with .authenticate
	smfOracle.authAnonymous();
	
	// logging an event
	smfOracle.logAndFlushAnalytics('Application_OnStart');
	
	initRequire("main.js");
}

function Application_OnError(e) {
	switch (e.type) {
		case "Server Error":
		case "Size Overflow":
			alert(lang.networkError);
			break;
		default:
			//change the following code for desired generic error messsage
			alert({
				title: lang.applicationError,
				message: e.message + "\n\n*" + e.sourceURL + "\n*" + e.line + "\n*" + e.stack
			});
			break;
	}
}

/* Animations */
var defaultPageAnimation = {
    motionEase: Device.deviceOS === "iOS" ? SMF.UI.MotionEase.NONE : SMF.UI.MotionEase.NONE,
    transitionEffect: Device.deviceOS === "iOS" ? SMF.UI.TransitionEffect.RIGHTTOLEFT : SMF.UI.TransitionEffect.NONE,
    transitionEffectType: Device.deviceOS === "iOS" ? SMF.UI.TransitionEffectType.PUSH : SMF.UI.TransitionEffectType.NONE,
    fade: Device.deviceOS === "iOS" ? false : true,
    reset: false,
    duration: 300
}
var reverseDefaultPageAnimation = {
    motionEase: Device.deviceOS === "iOS" ? SMF.UI.MotionEase.ACCELERATEANDDECELERATE : SMF.UI.MotionEase.NONE,
    transitionEffect: Device.deviceOS === "iOS" ? SMF.UI.TransitionEffect.LEFTTORIGHT : SMF.UI.TransitionEffect.NONE,
    transitionEffectType: Device.deviceOS === "iOS" ? SMF.UI.TransitionEffectType.PUSH : SMF.UI.TransitionEffectType.NONE,
    fade: Device.deviceOS === "iOS" ? false : true,
    reset: false,
    duration: 300
}