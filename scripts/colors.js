module.exports = {
    akSigortaRed: "#F10000",
    akSigortaWhite: "#FEFFFF",
    akSigortaRedLightest: "#FF5E5E",
    akSigortaRedLight: "#FF3232",
    akSigortaRedDark: "#BF0000",
    akSigortaRedDarkest: "#950000",
};
