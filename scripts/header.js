var colors = require("colors");
var header = new HeaderBar();
module.exports = header;

function HeaderBar() {
    this.navigationItem = null;
    this.actionBar = null;
    this.isAndroid = Device.deviceOS == "Android" ? true : false; //A control variable to check the environment is Android Operating System
    //Initilaizes actionbar / navigation item for the page which is provided with the parameter
    this.init = function(page) {
        //Sets ActctionBar for Android
        if (this.isAndroid == true) {
            this.actionBar = page.actionBar;
            this.actionBar.visible = true;
            this.actionBar.backgroundColor = "#dddddd";
            this.actionBar.alpha = "0.5";
            this.actionBar.overlay = true;
            this.actionBar.menuItems = [];
        }
        else {
            //Sets NavigationITem for iOS
            this.navigationItem = page.navigationItem;
            SMF.UI.iOS.NavigationBar.visible = true;
            SMF.UI.iOS.NavigationBar.translucent = true;
            SMF.UI.iOS.NavigationBar.tintColor = colors.akSigortaRed;
            this.navigationItem.rightBarButtonItems = [];
            this.navigationItem.leftBarButtonItems = [];
        }
    };
    //Sets the visitble Title text in ActibonBar/NavigationItem
    this.setTitle = function(title) {
        if (this.isAndroid == true) {
            this.actionBar.titleView = {
                type: SMF.UI.TitleViewType.TEXT,
                text: title,
                textSize: 18,
                alignment: SMF.UI.Alignment.CENTER
            };
        }
        else {
            this.navigationItem.title = title;
        }
    };
    //Sets the button on right side of the title with the provided text
    this.setRightItem = function(itemTitle, action) {
        function onAction(e) {}
        action = action || onAction;
        var key = {
                id: 0,
                title: itemTitle,
                onSelected: action,
                fontSize: 16
            },
            item;
        if (this.isAndroid && this.actionBar) {
            if (!key.title) {
                key.icon = "search.png";
                key.title = "Acenta Ara";
            }
            key.showAsAction = SMF.UI.Android.ShowAsAction.ALWAYS; //Always place this item in the Action Bar. Avoid using this unless it's critical that the item always appear in the action bar. Setting multiple items to always appear as action items can result in them overlapping with other UI in the action bar.
            item = new SMF.UI.Android.MenuItem(key);
            this.actionBar.menuItems = [item];
        }
        else if (!this.isAndroid) {
            if (!key.title) {
                key.systemItem = SMF.UI.iOS.BarButtonType.SEARCH;
            }
            item = new SMF.UI.iOS.BarButtonItem(key);
            this.navigationItem.rightBarButtonItems = [item];
        }
    };
    // Sets a pressible item to the left of the title
    this.setLeftItem = function(text, action) {

        if (this.isAndroid == true) {
            this.actionBar.displayHomeAsUpEnabled = true;
            this.actionBar.displayShowHomeEnabled = false;
            this.actionBar.title = text || "";
            this.actionBar.onHomeIconItemSelected = action || function() {
                alert("Home Button clicked");
            };
        }
        else {
            var leftItem = new SMF.UI.iOS.BarButtonItem({
                title: text || "",
                onSelected: action || function() {
                    alert("Left item clicked");
                }
            });
            this.navigationItem.leftBarButtonItems = [leftItem];
        }
    };
}
