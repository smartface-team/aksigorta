/* globals mcs */
var mcs_config = {
  "logLevel": mcs.logLevelInfo,
  "mobileBackends": {
    "smartfaceOracleMCS": {
      "default": true,
      "baseUrl": "https://smartface-mobilebel.mobileenv.em2.oraclecloud.com:443",
      "applicationKey": Device.deviceOS === "Android" ? "ee0f80b7-cfb6-4122-a710-0a5879c82d31" : "8911f626-6bae-4a8f-b9d3-56b904c0d1f3",
      "authorization": {
        "basicAuth": {
          "backendId": "8cc9bbe8-e817-4834-a943-057125eb16d2",
          "anonymousToken": "TU9CSUxFQkVMX1NNQVJURkFDRV9NT0JJTEVfQU5PTllNT1VTX0FQUElEOmZzOXEzakltbm9iX2hw"
        },
        "oAuth": {
          "clientId": "4b9c537d-f807-4921-a7a0-68ee7f6f7f03",
          "clientSecret": "DfdhIGVzzeOSRymPGlb7",
          "tokenEndpoint": "https://mobilebel.identity.europe.oraclecloud.com/oam/oauth2/tokens"
        },
        "facebookAuth": {
          "facebookAppId": "YOUR_FACEBOOK_APP_ID",
          "backendId": "YOUR_BACKEND_ID",
          "anonymousToken": "YOUR_BACKEND_ANONYMOUS_TOKEN"
        },
        "ssoAuth": {
          "clientId": "YOUR_CLIENT_ID",
          "clientSecret": "YOUR_ClIENT_SECRET",
          "tokenEndpoint": "YOUR_TOKEN_ENDPOINT"
        }
      }
    }
  }
};
