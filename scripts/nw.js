/* globals */
(function() {
    var nw = require("nw-smf");
    module.exports = nw;

    nw.commonHeaders["Content-Type"] = "application/json";

    nw.baseURL = "https://poc:M*9W2Yy$&_6fR9+e@testapi.aksigorta.com.tr/api/";

    var serviceDefinitions = ["authenticate", "changePassword",
        "kullaniciAcenteHiyerarsiSorgula", "resetPassword", "acenteUretimSorgula"
    ];
    for (var i in serviceDefinitions) {
        var serviceDefinitionPath = "./services/" + serviceDefinitions[i] +
            ".json";
        nw.registerService(require(serviceDefinitionPath));
    }
    nw.defaultAction = "mock"; // mock or run
})(); 
