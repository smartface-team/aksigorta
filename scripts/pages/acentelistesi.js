/* globals defaultPageAnimation, smfOracle*/
//TODO: include this file in onStart in pages/index.js Use the code below:
//include("pages/acentelistesi.js");
(function() {
    var header = require("header.js");
    var colors = require("colors.js");
    var getUnit = require("getUnit.js");
    var acente = {};
    var acentelistesi = [];
    var rboxDataRowRender = require("smf-rbox-helper").rboxDataRowRender;


    var pgAcentelistesi = Pages.pgAcentelistesi = new SMF.UI.Page({
        name: "pgAcentelistesi",
        onKeyPress: pgAcentelistesi_onKeyPress,
        onShow: pgAcentelistesi_onShow,
        fillColor: '#e7e7e7'
    });
    module.exports = pgAcentelistesi;

    /**
     * Creates action(s) that are run when the user press the key of the devices.
     * @param {KeyCodeEventArguments} e Uses to for key code argument. It returns e.keyCode parameter.
     * @this Pages.pgAcentelistesi
     */
    function pgAcentelistesi_onKeyPress(e) {
        if (e.keyCode === 4) {
            back();
        }
    }

    /**
     * Creates action(s) that are run when the page is appeared
     * @param {EventArguments} e Returns some attributes about the specified functions
     * @this Pages.acentelistesi
     */
    function pgAcentelistesi_onShow() {
        header.init(this);
        header.setTitle("Acente Seçimi");
        Device.deviceOS === "iOS" && header.setRightItem(null, switchSearch);
        header.setLeftItem("Çıkış", back);


        //We are going w/ dark mode. Our navbar is white.
        SMF.UI.statusBar.style = SMF.UI.StatusBarStyle.DEFAULT;
        search_bar.text = '';

        if (cntSearchBar.top === 0) {
            cntSearchBar.top = initialSearchbarShiftMinus;
            rbAcenteListesi.top = 0;
            rbAcenteListesi.height = cntMain.height;
        }
        smfOracle.logAndFlushAnalytics('pgAcentelistesi_onShow');
        rbAcenteListesi.selectedItemIndex = -1;
    }

    function showData(data) {
        //acente = data;
        search_bar.text = "";
        acentelistesi = data;
        refresh();
        this.show(defaultPageAnimation);
    }
    pgAcentelistesi.showData = showData;
    


    function back() {
        Pages.back(require("./login"));
    }

    function refresh() {
        var filterText = search_bar.text.toLocaleLowerCase() || "";
        var filteredData = acentelistesi.filter(function(acente) {
            var acenteAdi = acente.acenteAdi.toLocaleLowerCase();
            var result = acenteAdi.indexOf(filterText) > -1;
            if (!result)
                console.log("Filtering out \"" + acenteAdi + "\" due to not matching filter \"" + filterText + "\"");
            return result;
        });

        rbAcenteListesi.dataSource = filteredData;
        rbAcenteListesi.refresh();
        rbAcenteListesi.selectedItemIndex = -1;
    }

    var cntMain = new SMF.UI.Container({
        top: getUnit({
            iOS: 64,
            Android: 80
        }), //because ovearlay = true and nav/actionbar heights should be considered.
        left: 0,
        width: "100%",
        borderWidth: 0
    });
    pgAcentelistesi.add(cntMain);
    cntMain.height = Device.screenHeight - cntMain.top;

    var rbAcenteListesi = new SMF.UI.RepeatBox({
        useActiveItem: true,
        top: 0,
        left: 0,
        height: "100%",
        width: "100%",
        onRowRender: function(e) {
            rboxDataRowRender.call(this, e);
        },
        borderWidth: 0,
        visible: true,
        onSelectedItem: rbSelectedItem
    });
    cntMain.add(rbAcenteListesi);
    fillRBAcenteListesiTemplate(rbAcenteListesi.itemTemplate, false);
    fillRBAcenteListesiTemplate(rbAcenteListesi.activeItemTemplate, true);


    function fillRBAcenteListesiTemplate(template, isActiveItem) {
        var imgStatusCircle = new SMF.UI.Image({
            name: 'imgStatusCircle',
            image: 'white_circle.png',
            left: '3%',
            top: getUnit({
                iOS: ((((Device.screenHeight - 64) / 7) - 60) / 2),
                Android: 10
            }),
            width: getUnit(60),
            height: getUnit(60),
            imageFillType: SMF.UI.ImageFillType.ASPECTFIT
        });

        var lblStatusLetter = new SMF.UI.Label({
            name: 'lblStatusLetter',
            text: 'A',
            left: '3%',
            top: getUnit({
                iOS: ((((Device.screenHeight - 64) / 7) - 60) / 2),
                Android: 9
            }),
            width: getUnit(60),
            height: getUnit(60),
            textAlignment: SMF.UI.TextAlignment.CENTER,
            multipleLine: false,
            font: new SMF.UI.Font({
                size: "64px",
                bold: false
            }),
            fontColor: '#248afd',
            touchEnabled: false
        });
        lblStatusLetter.dataKey = "firstLetters";

        var recVerticalLine = new SMF.UI.Rectangle({
            name: 'recVerticalLine',
            left: getUnit('22%'),
            top: getUnit('17%'),
            width: getUnit(1),
            height: '71%',
            fillColor: '#979797',
            borderWidth: 0,
            roundedEdge: 0,
            touchEnabled: false
        });

        var lblName = new SMF.UI.Label({
            name: 'lblName',
            text: '-',
            left: '25%',
            top: getUnit({
                iOS: '9%',
                Android: '8%'
            }),
            width: '60%',
            height: '80%',
            textAlignment: SMF.UI.TextAlignment.LEFT,
            multipleLine: true,
            font: new SMF.UI.Font({
                size: '48px',
                bold: false
            }),
            fontColor: '#248afd',
            borderWidth: 0,
            touchEnabled: false
        });
        lblName.dataKey = "acenteAdi";

        // Baska bilgi eklenecekse bu iki label style olarak kullanilabilir

        // var lblCode = new SMF.UI.Label({
        //     name: 'lblCode',
        //     text: '-',
        //     left: '25%',
        //     top: getUnit({iOS:'46%',Android:'40%'}),
        //     width: '60%',
        //     height: getUnit({iOS:'20%',Android:'30%'}),
        //     textAlignment: SMF.UI.TextAlignment.LEFT,
        //     multipleLine: false,
        //     font: new SMF.UI.Font({
        //         size: '7pt',
        //         bold: false
        //     }),
        //     fontColor: '#4a4a4a',
        //     borderWidth: 0
        // });
        // lblCode.dataKey = "acenteKod";

        // var lblLeaveDetails = new SMF.UI.Label({
        //     name: 'lblLeaveDetails',
        //     text: '-',
        //     left: '25%',
        //     top: '63%',
        //     width: '60%',
        //     height: '30%',
        //     textAlignment: SMF.UI.TextAlignment.LEFT,
        //     multipleLine: false,
        //     font: new SMF.UI.Font({
        //         size: '7pt',
        //         bold: false
        //     }),
        //     fontColor: '#4a4a4a',
        //     borderWidth: 0
        // });

        var imgDetail = new SMF.UI.Image({
            name: 'imgDetail',
            image: 'right_arrow.png',
            left: '88%',
            top: '38%',
            width: '10%',
            height: '30%',
            imageFillType: SMF.UI.ImageFillType.NORMAL,
            touchEnabled: false
        });

        var recHorizontalLine = new SMF.UI.Rectangle({
            name: 'recHorizontalLine',
            left: getUnit(0),
            top: getUnit({
                iOS: ((Device.screenHeight - 64) / 7) - 1,
                Android: 79
            }),
            width: getUnit('100%'),
            height: 1,
            fillColor: '#FFFFFF',
            borderWidth: 0,
            roundedEdge: 0,
            touchEnabled: false
        });


        //adding files to repeatbox's itemtemplate
        template.height = getUnit({
            iOS: (Device.screenHeight - 64) / 7,
            Android: 80
        });
        template.add(imgStatusCircle);
        template.add(lblStatusLetter);
        template.add(recVerticalLine);
        template.add(lblName);
        // template.add(lblCode);
        // template.add(lblLeaveDetails);
        template.add(imgDetail);
        template.add(recHorizontalLine);
        template.fillColor = isActiveItem ? "#B2B2B2" : '#e7e7e7';
    }

    var initialSearchbarShift = 40;
    var initialSearchbarShiftMinus = initialSearchbarShift * -1;
    var cntSearchBar = new SMF.UI.Container({
        top: initialSearchbarShiftMinus,
        height: initialSearchbarShift,
        width: "100%",
        left: 0,
        borderWidth: 0
    });
    cntMain.add(cntSearchBar);

    var search_bar = new SMF.UI.SearchBar({
        name: 'search_bar',
        placeHolder: "Acente Adı",
        width: "100%",
        left: 0,
        top: 0,
        height: "100%",
        text: "",
        onSearchSubmit: function(e) {
            refresh();
        },
        onCancel: function(e) {
            search_bar.text = "";
            refresh();
        },
        onTextChange: function(e) {
            refresh();
        }
    });
    cntSearchBar.add(search_bar);
    search_bar.actionView = true;



    var animationDefaults = {
        property: "top",
        motionEase: SMF.UI.MotionEase.PLAIN,
        duration: 100,
        onFinish: function(e) {
            skipSwitchSearch = false;
        }
    };
    var skipSwitchSearch = false;

    function switchSearch() {
        if (skipSwitchSearch)
            return;
        skipSwitchSearch = true;
        if (cntSearchBar.top === 0) { //hide
            search_bar.cancel();
            cntSearchBar.animate(Object.assign({
                endValue: initialSearchbarShiftMinus
            }, animationDefaults));
            rbAcenteListesi.animate(Object.assign({
                endValue: 0
            }, animationDefaults));
            rbAcenteListesi.animate(Object.assign({
                endValue: "100%"
            }, animationDefaults, {
                property: "height",
                onFinish: undefined
            }));
        }
        else { //show
            cntSearchBar.animate(Object.assign({
                endValue: 0
            }, animationDefaults));
            rbAcenteListesi.animate(Object.assign({
                endValue: initialSearchbarShift
            }, animationDefaults));
            rbAcenteListesi.animate(Object.assign({
                endValue: cntMain.height - initialSearchbarShift
            }, animationDefaults, {
                property: "height",
                onFinish: undefined
            }));
        }
    }

    function rbSelectedItem(e) {
        if (e.rowIndex > -1) {
            var data = rbAcenteListesi.dataSource[e.rowIndex];
            //var prUretimSorgula = require("./uretimsorgula");
            //prUretimSorgula.showData(data);
            var uretimListesi1 = require("./uretimlistesi1");
            uretimListesi1.showData(data);
        }
    }

})();
