/* globals smfOracle*/

(function() {
	var nw = require("nw-smf");
	var colors = require("colors.js");
	var Dialog = require("smf-dialog");
	var dot = require('dot-object');
	var showServiceError = require("showServiceError");

	var pgLogin = Pages.pgLogin = new SMF.UI.Page({
		name: "pgLogin",
		fillColor: colors.akSigortaWhite,
		onKeyPress: page1_onKeyPress,
		onShow: page1_onShow,
		backgroundImage: 'stripe.png',
		imageFillType: SMF.UI.ImageFillType.TILE
	});
	module.exports = pgLogin;

	var imgHome = new SMF.UI.Image({
		name: "img",
		image: "home_back.png",
		left: 0,
		top: 0,
		width: "100%",
		height: "40%",
		imageFillType: SMF.UI.ImageFillType.STRETCH,
		onTouch: function(e) {
			var loginInfo = {
				"userName": "ISTA13006S1",
				"password": "C5LclziRhb"
			};
			tbPassword.text = loginInfo.password;
			tbUserName.text = loginInfo.userName;
		}
	});

	var lblWelcome = new SMF.UI.Label({
		top: "16%",
		left: "15%",
		width: "70%",
		height: "20%",
		text: "Acente Takip Sistemi",
		textAlignment: SMF.UI.TextAlignment.CENTER,
		font: new SMF.UI.Font({
			size: "15pt"
		}),
		fontColor: SMF.UI.Color.WHITE,
		touchEnabled: false,
		showScrollBar: false,
		multipleLine: true,
		borderWidth: 0
	});

	var lblWelcome2 = new SMF.UI.Label({
		top: "32%",
		left: "10%",
		width: "80%",
		height: "8%",
		text: "Powered by Smartface & Oracle",
		textAlignment: SMF.UI.TextAlignment.CENTER,
		font: new SMF.UI.Font({
			size: "5pt"
		}),
		fontColor: SMF.UI.Color.WHITE,
		touchEnabled: false,
		showScrollBar: false,
		borderWidth: 0
	});

	pgLogin.add(imgHome);
	pgLogin.add(lblWelcome);
	pgLogin.add(lblWelcome2);

	var textBoxDefaults = {
		left: "10%",
		width: "80%",
		height: "8%",
		text: "",
		horizontalGap: Device.deviceOS === "Android" ? "24dp" : "10dp",
		borderWidth: 1, //Device.deviceOS === "Android" ? 8 : 3,
		backgroundTransparent: false,
		fillColor: colors.akSigortaWhite,
		borderColor: colors.akSigortaWhite,
		activeBorderColor: colors.akSigortaRedLightest,
		roundedEdge: 1
	};

	var tbUserName = new SMF.UI.TextBox(
		Object.assign({}, textBoxDefaults, {
			top: "51%",
			placeHolder: "Kullanıcı Adı",
			returnKeyType: SMF.UI.TextBoxReturnKey.NEXT,
			onReturnKey: function(e) {
				tbPassword.focus();
			}
		})
	);

	pgLogin.add(tbUserName);

	var tbPassword = new SMF.UI.TextBox(
		Object.assign({}, textBoxDefaults, {
			top: "60%",
			placeHolder: "Şifre",
			isPassword: true,
			keyboardType: Device.deviceOS === "Android" ?
				SMF.UI.TextBox.KeyboardType.TEXTNOSUGGESTIONS : SMF.UI.TextBox.KeyboardType.DEFAULT,
			returnKeyType: SMF.UI.TextBoxReturnKey.DONE,
			onReturnKey: function(e) {
				btnLogin.onPressed(e);
			}
		}));
	pgLogin.add(tbPassword);

	var btnLogin = new SMF.UI.TextButton({
		top: "69%",
		left: "10%",
		width: "80%",
		height: "8%",
		text: "Oturum Aç",
		textAlignment: SMF.UI.TextAlignment.CENTER,
		onPressed: pgLogin_btnLogin_onPressed,
		roundedEdge: 1,
		fillColor: colors.akSigortaRed,
		pressedFillColor: colors.akSigortaRedDark,
		font: new SMF.UI.Font({
			size: "52px"
		})
	});
	(Device.deviceOS === "Android") && (btnLogin.effects.ripple.enabled = true);
	pgLogin.add(btnLogin);
	global.buttonHeight = btnLogin.height;

	var lblInfoText = new SMF.UI.Label({
		top: "78%",
		left: "10%",
		width: "80%",
		height: "8%",
		text: "Lütfen kullanıcı adı ve şifrenizi girin.",
		textAlignment: SMF.UI.TextAlignment.TOP,
		font: new SMF.UI.Font({
			size: "6pt"
		})
	});
	pgLogin.add(lblInfoText);

	var lblVersion = new SMF.UI.Label({
		top: "97%",
		left: "0%",
		width: "99%",
		height: "3%",
		text: 'v.' + Application.version,
		textAlignment: SMF.UI.TextAlignment.RIGHT,
		font: new SMF.UI.Font({
			size: "4pt"
		}),
		fontColor: SMF.UI.Color.BLACK,
		touchEnabled: false,
		showScrollBar: false,
		borderWidth: 0,
		multipleLine: false
	});
	pgLogin.add(lblVersion);


	/**
	 * Creates action(s) that are run when the user press the key of the devices.
	 * @param {KeyCodeEventArguments} e Uses to for key code argument. It returns e.keyCode parameter.
	 * @this Pages.Page1
	 */
	function page1_onKeyPress(e) {
		if (e.keyCode === 4) {
			Application.exit();
		}
	}

	/**
	 * Creates action(s) that are run when the page is appeared
	 * @param {EventArguments} e Returns some attributes about the specified functions
	 * @this Pages.Page1
	 */
	function page1_onShow() {
		//caching:
		require("./uretimlistesi1.js");
		require("./acentelistesi.js");

		//We are going w/ dark mode. Our navbar is white.
		SMF.UI.statusBar.style = SMF.UI.StatusBarStyle.LIGHTCONTENT;
		tbPassword.text = tbUserName.text = "";

		//Resgistering device to Google and Apple servers
		Notifications.remote.registerForPushNotifications({
			OnSuccess: function(e) {
				console.log("registerForPushNotifications Success:" + JSON.prune(e));


				var registrationID = Notifications.remote.token;
				console.log('registrationID: ' + JSON.stringify(registrationID));
				var appId = "io.smartface.poc";
				var appVersion = Application.version;
				console.log('Application.version: ' + JSON.stringify(Application.version));

				//Registering device to Oracle MCS
				console.log('trying to register smfOracle.registerNotification');
				smfOracle.registerNotification(registrationID, appId, appVersion, function(err) {
					if (err) {
						console.log('Oracle MCS registration of push notifications failed: ' + JSON.stringify(err));
					}
					else {
						console.log('Success! Registered for push notifications');
					}
				});

			},
			OnFailure: function(e) {
				console.log('Notifications.remote.registerForPushNotifications failed: ' + JSON.stringify(e));
			}
		});

		smfOracle.logAndFlushAnalytics('page1_onShow');
	}

	/**
	 * Creates action(s) that are run when the object is pressed from device's screen.
	 * @param {EventArguments} e Returns some attributes about the specified functions
	 * @this Page1.TextButton1
	 */
	function pgLogin_btnLogin_onPressed(e) {
		var loginInfo = {
			"userName": tbUserName.text,
			"password": tbPassword.text
		};

		if (!loginInfo.password || !loginInfo.userName) {
			alert({
				message: "Kullanıcı adı ve şifre girilmesi mecburidir"
			});
			return;
		}

		var kullaniciAcenteHiyerarsiSorgulaBody = {
			"kanalBilgileri": {
				"kanalId": "1",
				"branchId": "176",
				"token": "_h?xJ_u6?@MCr^z"
			},
			"kullaniciId": "29"
		};
		// var kullaniciId = "29";
		// Object.defineProperty(kullaniciAcenteHiyerarsiSorgulaBody, "kullaniciId", {
		// 	get: function() { return kullaniciId;},
		// 	set: function(value) { return kullaniciId = value;},
		// 	enumerable: true,
		// 	configurable: false
		// });

		Dialog.showWait();
		nw.factory("authenticate").body(loginInfo).result(function(err, data) {
			Dialog.removeWait();
			if (err) {
				alert("Unknown error occured");
			}
			else {
				if (Number(data.body.errorCode)) {
					data.next = false;
					return showServiceError(data);
				}
				kullaniciAcenteHiyerarsiSorgulaBody.kullaniciId = data.body.kullaniciId;
				this.chained.bodyValues = kullaniciAcenteHiyerarsiSorgulaBody;
			}
		}).chain("kullaniciAcenteHiyerarsiSorgula").
		body(kullaniciAcenteHiyerarsiSorgulaBody).
		result(function(err, data) {
			Dialog.removeWait();
			if (err) {
				alert("Unknown error occured");
			}
			else {
				if (Number(data.body.errorCode))
					return showServiceError(data);
				var flatData = dot.dot(data.body);
				var acenteListesi = [];
				for (var key in flatData) {
					var index = key.indexOf(".acenteAdi");
					if (index > -1) {
						var itemKey = key.substr(0, index);
						var item = JSON.parse(JSON.stringify(dot.pick(itemKey, data.body)));
						var words = item.acenteAdi.toLocaleUpperCase().split(" ", 2);
						item.firstLetters = "";
						for (var wI in words)
							item.firstLetters += words[wI][0];
						acenteListesi.push(item);
					}
				}
				if (acenteListesi.length === 1) {
					var pgUretimListesi1 = require("./uretimlistesi1");
					pgUretimListesi1.showData(Object.assign(acenteListesi[0], {
						single: true
					}));
				}
				else {
					var pgAcentelistesi = require("./acentelistesi");
					pgAcentelistesi.showData(acenteListesi);
				}
			}
		})[nw.defaultAction]();
	}
})();
