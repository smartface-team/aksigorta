/* globals defaultPageAnimation, smfOracle*/
//TODO: include this file in onStart in pages/index.js Use the code below:
//include("pages/acentelistesi.js");
(function() {
    var header = require("header.js");
    var colors = require("colors.js");
    var getUnit = require("getUnit.js");
    var uretimlistesi = [];
    var rboxDataRowRender = require("smf-rbox-helper").rboxDataRowRender;


    var pgUretimlistesi = Pages.pgUretimlistesi = new SMF.UI.Page({
        name: "pgUretimlistesi",
        onKeyPress: pguretimlistesi_onKeyPress,
        onShow: pgUretimlistesi_onShow,
        fillColor: '#e7e7e7'
    });
    module.exports = pgUretimlistesi;

    /**
     * Creates action(s) that are run when the user press the key of the devices.
     * @param {KeyCodeEventArguments} e Uses to for key code argument. It returns e.keyCode parameter.
     * @this Pages.pgUretimlistesi
     */
    function pguretimlistesi_onKeyPress(e) {
        if (e.keyCode === 4) {
            back();
        }
    }

    /**
     * Creates action(s) that are run when the page is appeared
     * @param {EventArguments} e Returns some attributes about the specified functions
     * @this Pages.acentelistesi
     */
    function pgUretimlistesi_onShow() {
        header.init(this);
        header.setTitle("Branş Üretimleri");
        header.setLeftItem("Sorgula", back);


        //We are going w/ dark mode. Our navbar is white.
        SMF.UI.statusBar.style = SMF.UI.StatusBarStyle.DEFAULT;

        if (cntSearchBar.top === 0) {
            cntSearchBar.top = initialSearchbarShiftMinus;
            rbAcenteListesi.top = 0;
            rbAcenteListesi.height = cntMain.height;
        }
        smfOracle.logAndFlushAnalytics('pgUretimlistesi_onShow');
    }

    function showData(data) {
        uretimlistesi = data;
        refresh();
        this.show(defaultPageAnimation);
        console.log("branş üretimleri gösterildi");
    }
    pgUretimlistesi.showData = showData;


    function back() {
        Pages.back(require("./uretimsorgula"));
    }

    function refresh() {
        var filteredData = uretimlistesi;

        rbAcenteListesi.dataSource = filteredData;
        rbAcenteListesi.refresh();
        rbAcenteListesi.selectedItemIndex = -1;
    }

    var cntMain = new SMF.UI.Container({
        top: getUnit({
            iOS: 64,
            Android: 80
        }), //because ovearlay = true and nav/actionbar heights should be considered.
        left: 0,
        width: "100%",
        borderWidth: 0
    });
    pgUretimlistesi.add(cntMain);
    cntMain.height = Device.screenHeight - cntMain.top;

    var rbAcenteListesi = new SMF.UI.RepeatBox({
        useActiveItem: false,
        top: 0,
        left: 0,
        height: "100%",
        width: "100%",
        onRowRender: function(e) {
            /*setTimeout(function() {*/
            // console.log(JSON.stringify(global.getRect(), null, "\t")) /*}, 10000)*/ ;
            rboxDataRowRender.call(this, e);
        },
        borderWidth: 0,
        visible: true,
        // onSelectedItem: rbSelectedItem
    });
    cntMain.add(rbAcenteListesi);
    fillRBAcenteListesiTemplate(rbAcenteListesi.itemTemplate, false);
    // fillRBAcenteListesiTemplate(rbAcenteListesi.activeItemTemplate, true);


    function fillRBAcenteListesiTemplate(template, isActiveItem) {
        var templateHeightValue = 0;
        var activeItemMultiplier = 3;
        var regularWidth = 69;
        if (Device.deviceOS === "Android")
            templateHeightValue = 92;
        else if (Device.deviceOS === "iOS")
            templateHeightValue = (Device.screenHeight - 64) / 7;
        // if (isActiveItem) {
            templateHeightValue = templateHeightValue * activeItemMultiplier;
        // }


        var imgStatusCircle = new SMF.UI.Image({
            name: 'imgStatusCircle',
            image: 'white_circle.png',
            left: '3%',
            top: getUnit({
                iOS: ((((Device.screenHeight - 64) / 7) - 60) / 2),
                Android: 10
            }),
            width: getUnit(60),
            height: getUnit(60),
            imageFillType: SMF.UI.ImageFillType.ASPECTFIT
        });

        var lblStatusLetter = new SMF.UI.Label({
            name: 'lblStatusLetter',
            text: 'A',
            left: '3%',
            top: getUnit({
                iOS: ((((Device.screenHeight - 64) / 7) - 60) / 2),
                Android: 9
            }),
            width: getUnit(60),
            height: getUnit(60),
            textAlignment: SMF.UI.TextAlignment.CENTER,
            multipleLine: false,
            font: new SMF.UI.Font({
                size: Device.deviceOS === "iOS" ?
                    '15pt' : "11pt",
                bold: false,
                name: "FontAwesome"
            }),
            fontColor: '#248afd',
            touchEnabled: false
        });
        lblStatusLetter.dataKey = "icon";

        var recVerticalLine = new SMF.UI.Rectangle({
            name: 'recVerticalLine',
            left: getUnit('22%'),
            top: getUnit({
                iOS: 9,
                Android: 5
            }),
            width: getUnit(1),
            height: getUnit(70),
            fillColor: '#979797',
            borderWidth: 0,
            roundedEdge: 0,
            touchEnabled: false
        });

        var lblTitle = new SMF.UI.Label({
            name: 'lblName',
            text: '-',
            left: '25%',
            top: getUnit({
                iOS: 9,
                Android: 5
            }),
            width: regularWidth + '%',
            height: getUnit(39),
            textAlignment: SMF.UI.TextAlignment.LEFT,
            multipleLine: false,
            font: new SMF.UI.Font({
                size: Device.deviceOS === "iOS" ? '12pt' : "9pt",
                bold: false
            }),
            fontColor: '#248afd',
            borderWidth: 0,
            touchEnabled: false
        });
        lblTitle.dataKey = "title";

        var recHorizontalLine = new SMF.UI.Rectangle({
            name: 'recHorizontalLine',
            left: getUnit(0),
            top: getUnit(templateHeightValue - 1),
            width: getUnit('100%'),
            height: 1,
            fillColor: '#FFFFFF',
            borderWidth: 0,
            roundedEdge: 0,
            touchEnabled: false
        });


        var rectSuccessBackground = new SMF.UI.Rectangle({
            name: "rectSuccessBackground",
            fillColor: "black",
            left: "25%",
            top: getUnit({
                iOS: 57,
                Android: 45
            }),
            height: getUnit(20),
            touchEnabled: false,
            width: regularWidth + "%",
            borderWidth: 0
        });

        var rectSuccessRate = new SMF.UI.Rectangle({
            name: "rectSuccessBackground",
            fillColor: colors.akSigortaRed,
            left: "25%",
            top: getUnit({
                iOS: 57,
                Android: 45
            }),
            height: getUnit(20),
            touchEnabled: false,
            width: 0,
            borderWidth: 0
        });
        rectSuccessRate.dataKey = "successRate";
        rectSuccessRate.setData = function setRectSucessRateData(data, e) {
            rectSuccessRate.width = (regularWidth * data) + "%";
        };

        // var lblFields = new SMF.UI.Label({
        //     name: 'lblName',
        //     text: '-',
        //     left: '25%',
        //     top: getUnit({
        //         iOS: 97,
        //         Android: 83
        //     }),
        //     width: regularWidth + '%',
        //     height: getUnit(39 * 3.5),
        //     textAlignment: SMF.UI.TextAlignment.LEFT,
        //     multipleLine: true,
        //     font: new SMF.UI.Font({
        //         size: Device.deviceOS === "iOS" ? '11pt' : "8pt",
        //         bold: false
        //     }),
        //     fontColor: 'black',
        //     borderWidth: 0,
        //     touchEnabled: false,
        //     backgroundTransparent: true,
        //     fillColor: "green"
        // });
        // lblFields.dataKey = "fieldMega";

        // var lblOncekiDonem = new SMF.UI.Label({
        //     name: 'lblName',
        //     text: '-',
        //     left: '25%',
        //     top: getUnit({
        //         iOS: 243,
        //         Android: 200
        //     }),
        //     width: regularWidth + '%',
        //     height: getUnit(78),
        //     textAlignment: SMF.UI.TextAlignment.LEFT,
        //     multipleLine: true,
        //     font: new SMF.UI.Font({
        //         size: Device.deviceOS === "iOS" ? '11pt' : "8pt",
        //         bold: false
        //     }),
        //     fontColor: '#696969',
        //     borderWidth: 0,
        //     touchEnabled: false,
        //     backgroundTransparent: true,
        //     fillColor: "#4169e1"
        // });
        // lblOncekiDonem.dataKey = "field6";

        var addCounter = 0,
            addGap = 20;
        var lblFieldDefaults = {
            touchEnabled: false,
            height: getUnit(addGap),
            font: new SMF.UI.Font({
                size: Device.deviceOS === "iOS" ? "8pt" : "6pt"
            }),
            multipleLine: false
        };


        addKeyValue(getUnit({
            iOS: 97 + (addGap * addCounter),
            Android: 83 + (addGap * addCounter)
        }), "Gerçekleşen Pirim", "toplamBransGerceklesenPrimTutari");

        addKeyValue(getUnit({
            iOS: 97 + (addGap * addCounter),
            Android: 83 + (addGap * addCounter)
        }), "Hedeflenen Pirim", "toplamBransHedefPrimTutari");

        addKeyValue(getUnit({
            iOS: 97 + (addGap * addCounter),
            Android: 83 + (addGap * addCounter)
        }), "Gerçekleşen adet", "oncekiToplamBransAdet");

        addKeyValue(getUnit({
            iOS: 97 + (addGap * addCounter),
            Android: 83 + (addGap * addCounter)
        }), "Gerçekleşen komisyon tutarı", "toplamBransGerceklesenKomisyonTutari");

        addKeyValue(getUnit({
            iOS: 97 + (addGap * addCounter),
            Android: 83 + (addGap * addCounter)
        }), "Komisyon oranı", "toplamBransKomisyonOrani2");

        addKeyValue(getUnit({
            iOS: 97 + (addGap * addCounter),
            Android: 83 + (addGap * addCounter)
        }), "Yeni iş poliçe adedi", "toplamBransYeniIsPoliceAdet");

        addKeyValue(getUnit({
            iOS: 97 + (addGap * addCounter),
            Android: 83 + (addGap * addCounter)
        }), "Yeninelen poliçe adedi", "toplamBransYenilemePoliceAdet");

        addKeyValue(getUnit({
            iOS: 97 + (addGap * addCounter),
            Android: 83 + (addGap * addCounter)
        }), "Önceki dönem prim tutarı", "oncekiToplamBransPrimTutari", true);


        addKeyValue(getUnit({
            iOS: 97 + (addGap * addCounter),
            Android: 83 + (addGap * addCounter)
        }), "Önceki dönem adet", "oncekiToplamBransAdet", true);

        //adding files to repeatbox's itemtemplate
        template.height = getUnit(templateHeightValue);
        template.add(imgStatusCircle);
        template.add(lblStatusLetter);
        template.add(recVerticalLine);
        template.add(lblTitle);
        template.add(recHorizontalLine);
        template.add(rectSuccessBackground);
        template.add(rectSuccessRate);
        // template.add(lblFields);
        // template.add(lblOncekiDonem);
        template.fillColor = '#e7e7e7';

        function addKeyValue(top, keyName, dataKey, olderInformation) {
            var lblKey = new SMF.UI.Label(Object.assign({}, lblFieldDefaults, {
                top: top,
                left: "3%",
                width: "50%",
                text: keyName.endsWith(":") ? keyName : keyName + ":",
                textAlignment: SMF.UI.TextAlignment.LEFT,
                fontColor: olderInformation ? '#999999' : "black"
            }));
            template.add(lblKey);

            var lblValue = new SMF.UI.Label(Object.assign({}, lblFieldDefaults, {
                top: top,
                left: "53%",
                width: "42%",
                text: "-",
                textAlignment: SMF.UI.TextAlignment.RIGHT,
                fontColor: olderInformation ? '#999999' : "black"
            }));
            lblValue.dataKey = dataKey;
            template.add(lblValue);
            addCounter++;
        }
    }



    var initialSearchbarShift = 40;
    var initialSearchbarShiftMinus = initialSearchbarShift * -1;
    var cntSearchBar = new SMF.UI.Container({
        top: initialSearchbarShiftMinus,
        height: initialSearchbarShift,
        width: "100%",
        left: 0,
        borderWidth: 0
    });
    cntMain.add(cntSearchBar);





})();
