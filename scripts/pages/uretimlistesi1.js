/* globals defaultPageAnimation, smfOracle*/
//TODO: include this file in onStart in pages/index.js Use the code below:
//include("pages/acentelistesi.js");
(function() {
    var header = require("header.js");
    var nw = require("nw-smf");
    var Dialog = require("smf-dialog");
    var showServiceError = require("showServiceError");
    var colors = require("colors.js");
    var getUnit = require("getUnit.js");
    var uretimlistesi = [];
    var acente = {};
    var rboxDataRowRender = require("smf-rbox-helper").rboxDataRowRender;
    var baslangicTarihi = 201601,
        bitisTarihi = 201612,
        periyot = "";
    var periyotValues = ["", "G", "A", "Y"];

    var pgUretimlistesi1 = Pages.pgUretimlistesi1 = new SMF.UI.Page({
        name: "pgUretimlistesi1",
        onKeyPress: pgUretimlistesi1_onKeyPress,
        onShow: pgUretimlistesi1_onShow,
        fillColor: '#e7e7e7',
        backgroundImage: 'stripe.png',
        imageFillType: SMF.UI.ImageFillType.TILE
    });
    module.exports = pgUretimlistesi1;


    var presets = [{
        unselected: {
            fillColor: "#F5F5F5",
            fontColor: colors.akSigortaRed
        },
        selected: {
            fillColor: colors.akSigortaRed,
            fontColor: "#F5F5F5"
        },
        borderColor: "#DCDCDC"
    }, {
        unselected: {
            fillColor: colors.akSigortaRedDark,
            fontColor: "white"
        },
        selected: {
            fillColor: "white",
            fontColor: colors.akSigortaRed
        },
        borderColor: "white"
    }];
    var preset = presets[0];
    /**
     * Creates action(s) that are run when the user press the key of the devices.
     * @param {KeyCodeEventArguments} e Uses to for key code argument. It returns e.keyCode parameter.
     * @this Pages.pgUretimlistesi1
     */
    function pgUretimlistesi1_onKeyPress(e) {
        if (e.keyCode === 4) {
            back();
        }
    }

    /**
     * Creates action(s) that are run when the page is appeared
     * @param {EventArguments} e Returns some attributes about the specified functions
     * @this Pages.acentelistesi
     */
    function pgUretimlistesi1_onShow() {
        header.init(this);
        header.setTitle("Branş Üretimleri");
        var leftItemText = "";
        if (acente.single) {
            leftItemText = "Çıkış";
        }
        else {
            leftItemText = "Acenteler";
        }
        header.setLeftItem(leftItemText, back);



        //We are going w/ dark mode. Our navbar is white.
        SMF.UI.statusBar.style = SMF.UI.StatusBarStyle.DEFAULT;

        if (cntSearchBar.top === 0) {
            cntSearchBar.top = initialSearchbarShiftMinus;
            rbAcenteListesi.top = 0;
            rbAcenteListesi.height = cntMain.height;
        }
        smfOracle.logAndFlushAnalytics('pgUretimlistesi1_onShow');
    }

    function showData(data) {
        acente = data;
        lblAcenteAdi.text = acente.acenteAdi;
        fetch();
        this.show(defaultPageAnimation);
        console.log("branş üretimleri gösterildi");
    }
    pgUretimlistesi1.showData = showData;

    function fetch() {
        //uretimlistesi = data;
        //refresh();

        Dialog.showWait();
        /*alert(JSON.stringify({
            "kanalBilgileri": {
                "kanalId": "1",
                "branchId": "176",
                "token": "_h?xJ_u6?@MCr^z"
            },
            "subeKod": acente.subeKod,
            "kaynakKod": acente.subeKod,
            "acenteNo": acente.acenteKod,
            "sorgulamaBaslamaTarihi": baslangicTarihi,
            "sorgulamaBitisTarihi": bitisTarihi,
            "sorgulamaPeriyot": periyot
        }, null, "\t", "body"));*/


        nw.factory("acenteUretimSorgula").body({
            "kanalBilgileri": {
                "kanalId": "1",
                "branchId": "176",
                "token": "_h?xJ_u6?@MCr^z"
            },
            "subeKod": acente.subeKod,
            "kaynakKod": acente.subeKod,
            "acenteNo": acente.acenteKod,
            "sorgulamaBaslamaTarihi": baslangicTarihi,
            "sorgulamaBitisTarihi": bitisTarihi,
            "sorgulamaPeriyot": periyot
        }).result(function(err, data) {
            Dialog.removeWait();
            if (err) {
                //alert(JSON.stringify(err, null, "\t"), "error");
                alert("Unknown error occured");
                return;
            }
            if (Number(data.body.errorCode))
                return showServiceError(data);
            var bransUretimleri = data.body.bransUretimListesiType.bransUretimleri;
            var myStrings = ["(YOK)", "Yıllık", "Aylık", "Günlük"];
            var values = ["", "Y", "A", "G"];
            for (var i in bransUretimleri) {
                var bu = bransUretimleri[i];
                switch (bu.gruplamaAdi) {
                    case "KASKO":
                        bu.icon = ""; //shield f132
                        break;
                    case "KONUT":
                        bu.icon = ""; //home f015
                        break;
                    default:
                        bu.icon = ""; //global f0ac
                        // code
                }
                bu.toplamBransKomisyonOrani2 = String(Math.round(bu.toplamBransKomisyonOrani * 10) / 10).substr(0, 4);
                bu.title = bu.gruplamaAdi[0] +
                    bu.gruplamaAdi.substr(1).toLocaleLowerCase() + " (" +
                    myStrings[values.indexOf(bu.periyot)] + ": " +
                    String(bu.yilAy).substr(4, 2) + "." +
                    String(bu.yilAy).substr(0, 4) + ")";
                bu.field1 = "Prim Tutari: " +
                    bu.toplamBransGerceklesenPrimTutari + " /" +
                    bu.toplamBransHedefPrimTutari + " TL";
                bu.field2 = "Gerçekleşen adet: " + bu.toplamBransGerceklesenAdet;
                bu.field3 = "Komisyon: " + bu.toplamBransGerceklesenKomisyonTutari +
                    "TL (%" + bu.toplamBransKomisyonOrani2 + ")";
                bu.field4 = "Yeni iş poliçe adedi: " + bu.toplamBransYeniIsPoliceAdet;
                bu.field5 = "Yeninelen poliçe adedi: " + bu.toplamBransYenilemePoliceAdet;
                bu.field6 = "Önceki dönem:\n\tPrim tutarı - " + bu.oncekiToplamBransPrimTutari + "\n\tAdet - " + bu.oncekiToplamBransAdet;
                bu.successRate = bu.toplamBransGerceklesenPrimTutari / bu.toplamBransHedefPrimTutari;
                if (bu.successRate > 1)
                    bu.successRate = 1;
                else if (bu.successRate < 0)
                    bu.successRate = 0;
                bu.fieldMega = [bu.field1, bu.field2, bu.field3, bu.field4, bu.field5].join("\n");

            }

            uretimlistesi = bransUretimleri;
            refresh();

        })[nw.defaultAction]();
    }


    function back() {
        var prevPage = null;
        if (acente.single) {
            prevPage = require("./login");
        }
        else {
            prevPage = require("./acentelistesi");
        }

        Pages.back(prevPage);
    }

    function refresh() {
        var filteredData = uretimlistesi;

        rbAcenteListesi.dataSource = filteredData;
        rbAcenteListesi.refresh();
        rbAcenteListesi.selectedItemIndex = -1;
    }

    var gapV = 5,
        unit1 = 1;
    if (Device.deviceOS === "Android") {
        gapV = (gapV * Device.screenDpi) / 160;
        unit1 = (unit1 * Device.screenDpi) / 160;
    }

    var cntMain = new SMF.UI.Container({
        top: getUnit({
            iOS: 64,
            Android: 80
        }), //because ovearlay = true and nav/actionbar heights should be considered.
        left: 0,
        width: "100%",
        borderWidth: 0
    });
    pgUretimlistesi1.add(cntMain);
    cntMain.height = Device.screenHeight - cntMain.top;


    var lblAcenteAdi = new SMF.UI.Label({
        top: 0,
        left: 0,
        width: "100%",
        height: getUnit(40),
        fillColor: colors.akSigortaRedLight,
        backgroundTransparent: false,
        fontColor: "white",
        font: new SMF.UI.Font({
            size: Device.deviceOS === "iOS" ? "10pt" : "8pt"
        }),
        textAlignment: SMF.UI.TextAlignment.CENTER,
        roundedEdge: 0
    });
    cntMain.add(lblAcenteAdi);
    lblAcenteAdi.bottom = lblAcenteAdi.top + lblAcenteAdi.height;

    var rectTabBackground = new SMF.UI.Rectangle({
        borderWidth: 1,
        borderColor: preset.borderColor,
        fillColor: "white",
        roundedEdge: 4
    });
    cntMain.add(rectTabBackground);

    var cntTab = new SMF.UI.Container({
        top: lblAcenteAdi.bottom + gapV,
        left: "5%",
        width: "90%",
        roundedEdge: 4,
        height: getUnit(45),
        fillColor: "#F5F5F5",
        backgroundTransparent: true,
        borderWidth: 0,
        borderColor: "#DCDCDC"
    });
    cntMain.add(cntTab);

    cntTab.bottom = cntTab.top + cntTab.height;
    rectTabBackground.top = cntTab.top - unit1;
    rectTabBackground.height = cntTab.height + (unit1 * 2);
    rectTabBackground.left = cntTab.left - unit1;
    rectTabBackground.width = cntTab.width + (unit1 * 2);



    var tabLabels = [];

    function addTabItem(text, order) {
        var rectBackLbl = new SMF.UI.Rectangle({
            top: 0,
            height: "100%",
            width: (order === 1 ? 34 : 16) + "%",
            fillColor: preset.unselected.fillColor,
            borderWidth: 0,
            roundedEdge: 0
        });
        switch (order) {
            case 0:
                rectBackLbl.left = "17%";
                break;
            case 1:
                rectBackLbl.left = "33%";
                break;
            case 2:
                rectBackLbl.left = "67%";
                break;
        }
        cntTab.add(rectBackLbl);
        var lbl = new SMF.UI.Label({
            touchEnabled: true,
            width: order === 1 ? "34%" : "33%",
            top: 0,
            height: "100%",
            text: text,
            textAlignment: SMF.UI.TextAlignment.CENTER,
            backgroundTransparent: false,
            borderWidth: 0,
            fillColor: preset.unselected.fillColor,
            fontColor: preset.unselected.fontColor,
            roundedEdge: rectTabBackground.roundedEdge,
            onTouchEnded: function(e) {
                var newPeriyot = periyotValues[order + 1];
                if (periyot === newPeriyot)
                    periyot = "";
                else
                    periyot = newPeriyot;
                for (var i in tabLabels) {
                    var c = tabLabels[i];
                    var style = c.periyot === periyot ?
                        preset.selected : preset.unselected;
                    c.fillColor = c.rect.fillColor =
                        style.fillColor;
                    c.fontColor = style.fontColor;
                }
                fetch();
            }
        });
        switch (order) {
            case 0:
                lbl.left = 0;
                break;
            case 1:
                lbl.left = "33%";
                break;
            case 2:
                lbl.left = "67%";
                break;
        }
        lbl.periyot = periyotValues[order + 1];
        lbl.rect = rectBackLbl;
        cntTab.add(lbl);
        tabLabels.push(lbl);



    }
    addTabItem("Günlük", 0);
    addTabItem("Aylık", 1);
    addTabItem("Yıllık", 2);

    var rectTabVLine1 = new SMF.UI.Rectangle({
        width: getUnit(1),
        height: "100%",
        top: 0,
        left: "33%",
        borderWidth: 0,
        fillColor: rectTabBackground.borderColor,
        touchEnabled: false,
        roundedEdge: 0
    });
    cntTab.add(rectTabVLine1);
    var rectTabVLine2 = rectTabVLine1.clone();
    rectTabVLine2.left = "67%";
    cntTab.add(rectTabVLine2);

    var rbAcenteListesi = new SMF.UI.RepeatBox({
        useActiveItem: false,
        top: cntTab.bottom + gapV,
        left: 0,
        height: cntMain.height - cntTab.bottom,
        width: "100%",
        onRowRender: function(e) {
            /*setTimeout(function() {*/
            // console.log(JSON.stringify(global.getRect(), null, "\t")) /*}, 10000)*/ ;
            rboxDataRowRender.call(this, e);
        },
        borderWidth: 0,
        visible: true,
        // onSelectedItem: rbSelectedItem
    });
    cntMain.add(rbAcenteListesi);
    fillRBAcenteListesiTemplate(rbAcenteListesi.itemTemplate, false);
    // fillRBAcenteListesiTemplate(rbAcenteListesi.activeItemTemplate, true);


    function fillRBAcenteListesiTemplate(template, isActiveItem) {
        var templateHeightValue = 0;
        var activeItemMultiplier = 3;
        var regularWidth = 69;
        if (Device.deviceOS === "Android")
            templateHeightValue = 92;
        else if (Device.deviceOS === "iOS")
            templateHeightValue = (Device.screenHeight - 64) / 7;
        // if (isActiveItem) {
        templateHeightValue = templateHeightValue * activeItemMultiplier;
        // }


        var imgStatusCircle = new SMF.UI.Image({
            name: 'imgStatusCircle',
            image: 'white_circle.png',
            left: '3%',
            top: getUnit({
                iOS: ((((Device.screenHeight - 64) / 7) - 60) / 2),
                Android: 10
            }),
            width: getUnit(60),
            height: getUnit(60),
            imageFillType: SMF.UI.ImageFillType.ASPECTFIT
        });

        var lblStatusLetter = new SMF.UI.Label({
            name: 'lblStatusLetter',
            text: 'A',
            left: '3%',
            top: getUnit({
                iOS: ((((Device.screenHeight - 64) / 7) - 60) / 2),
                Android: 9
            }),
            width: getUnit(60),
            height: getUnit(60),
            textAlignment: SMF.UI.TextAlignment.CENTER,
            multipleLine: false,
            font: new SMF.UI.Font({
                size: Device.deviceOS === "iOS" ?
                    '15pt' : "11pt",
                bold: false,
                name: "FontAwesome"
            }),
            fontColor: '#248afd',
            touchEnabled: false
        });
        lblStatusLetter.dataKey = "icon";

        var recVerticalLine = new SMF.UI.Rectangle({
            name: 'recVerticalLine',
            left: getUnit('22%'),
            top: getUnit({
                iOS: 9,
                Android: 5
            }),
            width: getUnit(1),
            height: getUnit(70),
            fillColor: '#979797',
            borderWidth: 0,
            roundedEdge: 0,
            touchEnabled: false
        });

        var lblTitle = new SMF.UI.Label({
            name: 'lblName',
            text: '-',
            left: '25%',
            top: getUnit({
                iOS: 9,
                Android: 5
            }),
            width: regularWidth + '%',
            height: getUnit(39),
            textAlignment: SMF.UI.TextAlignment.LEFT,
            multipleLine: false,
            font: new SMF.UI.Font({
                size: Device.deviceOS === "iOS" ? '12pt' : "9pt",
                bold: false
            }),
            fontColor: '#248afd',
            borderWidth: 0,
            touchEnabled: false
        });
        lblTitle.dataKey = "title";

        var recHorizontalLine = new SMF.UI.Rectangle({
            name: 'recHorizontalLine',
            left: getUnit(0),
            top: getUnit(templateHeightValue - 1),
            width: getUnit('100%'),
            height: 1,
            fillColor: '#FFFFFF',
            borderWidth: 0,
            roundedEdge: 0,
            touchEnabled: false
        });


        var rectSuccessBackground = new SMF.UI.Rectangle({
            name: "rectSuccessBackground",
            fillColor: "black",
            left: "25%",
            top: getUnit({
                iOS: 57,
                Android: 45
            }),
            height: getUnit(20),
            touchEnabled: false,
            width: regularWidth + "%",
            borderWidth: 0
        });

        var rectSuccessRate = new SMF.UI.Rectangle({
            name: "rectSuccessBackground",
            fillColor: colors.akSigortaRed,
            left: "25%",
            top: getUnit({
                iOS: 57,
                Android: 45
            }),
            height: getUnit(20),
            touchEnabled: false,
            width: 0,
            borderWidth: 0
        });
        rectSuccessRate.dataKey = "successRate";
        rectSuccessRate.setData = function setRectSucessRateData(data, e) {
            rectSuccessRate.width = (regularWidth * data) + "%";
        };

        // var lblFields = new SMF.UI.Label({
        //     name: 'lblName',
        //     text: '-',
        //     left: '25%',
        //     top: getUnit({
        //         iOS: 97,
        //         Android: 83
        //     }),
        //     width: regularWidth + '%',
        //     height: getUnit(39 * 3.5),
        //     textAlignment: SMF.UI.TextAlignment.LEFT,
        //     multipleLine: true,
        //     font: new SMF.UI.Font({
        //         size: Device.deviceOS === "iOS" ? '11pt' : "8pt",
        //         bold: false
        //     }),
        //     fontColor: 'black',
        //     borderWidth: 0,
        //     touchEnabled: false,
        //     backgroundTransparent: true,
        //     fillColor: "green"
        // });
        // lblFields.dataKey = "fieldMega";

        // var lblOncekiDonem = new SMF.UI.Label({
        //     name: 'lblName',
        //     text: '-',
        //     left: '25%',
        //     top: getUnit({
        //         iOS: 243,
        //         Android: 200
        //     }),
        //     width: regularWidth + '%',
        //     height: getUnit(78),
        //     textAlignment: SMF.UI.TextAlignment.LEFT,
        //     multipleLine: true,
        //     font: new SMF.UI.Font({
        //         size: Device.deviceOS === "iOS" ? '11pt' : "8pt",
        //         bold: false
        //     }),
        //     fontColor: '#696969',
        //     borderWidth: 0,
        //     touchEnabled: false,
        //     backgroundTransparent: true,
        //     fillColor: "#4169e1"
        // });
        // lblOncekiDonem.dataKey = "field6";

        var addCounter = 0,
            addGap = 20;
        var lblFieldDefaults = {
            touchEnabled: false,
            height: getUnit(addGap),
            font: new SMF.UI.Font({
                size: Device.deviceOS === "iOS" ? "8pt" : "6pt"
            }),
            multipleLine: false
        };


        addKeyValue(getUnit({
            iOS: 97 + (addGap * addCounter),
            Android: 83 + (addGap * addCounter)
        }), "Gerçekleşen Pirim", "toplamBransGerceklesenPrimTutari");

        addKeyValue(getUnit({
            iOS: 97 + (addGap * addCounter),
            Android: 83 + (addGap * addCounter)
        }), "Hedeflenen Pirim", "toplamBransHedefPrimTutari");

        addKeyValue(getUnit({
            iOS: 97 + (addGap * addCounter),
            Android: 83 + (addGap * addCounter)
        }), "Gerçekleşen adet", "oncekiToplamBransAdet");

        addKeyValue(getUnit({
            iOS: 97 + (addGap * addCounter),
            Android: 83 + (addGap * addCounter)
        }), "Gerçekleşen komisyon tutarı", "toplamBransGerceklesenKomisyonTutari");

        addKeyValue(getUnit({
            iOS: 97 + (addGap * addCounter),
            Android: 83 + (addGap * addCounter)
        }), "Komisyon oranı", "toplamBransKomisyonOrani2");

        addKeyValue(getUnit({
            iOS: 97 + (addGap * addCounter),
            Android: 83 + (addGap * addCounter)
        }), "Yeni iş poliçe adedi", "toplamBransYeniIsPoliceAdet");

        addKeyValue(getUnit({
            iOS: 97 + (addGap * addCounter),
            Android: 83 + (addGap * addCounter)
        }), "Yeninelen poliçe adedi", "toplamBransYenilemePoliceAdet");

        addKeyValue(getUnit({
            iOS: 97 + (addGap * addCounter),
            Android: 83 + (addGap * addCounter)
        }), "Önceki dönem prim tutarı", "oncekiToplamBransPrimTutari", true);


        addKeyValue(getUnit({
            iOS: 97 + (addGap * addCounter),
            Android: 83 + (addGap * addCounter)
        }), "Önceki dönem adet", "oncekiToplamBransAdet", true);

        //adding files to repeatbox's itemtemplate
        template.height = getUnit(templateHeightValue);
        template.add(imgStatusCircle);
        template.add(lblStatusLetter);
        template.add(recVerticalLine);
        template.add(lblTitle);
        template.add(recHorizontalLine);
        template.add(rectSuccessBackground);
        template.add(rectSuccessRate);
        // template.add(lblFields);
        // template.add(lblOncekiDonem);
        template.fillColor = '#e7e7e7';

        function addKeyValue(top, keyName, dataKey, olderInformation) {
            var lblKey = new SMF.UI.Label(Object.assign({}, lblFieldDefaults, {
                top: top,
                left: "3%",
                width: "50%",
                text: keyName.endsWith(":") ? keyName : keyName + ":",
                textAlignment: SMF.UI.TextAlignment.LEFT,
                fontColor: olderInformation ? '#999999' : "black"
            }));
            template.add(lblKey);

            var lblValue = new SMF.UI.Label(Object.assign({}, lblFieldDefaults, {
                top: top,
                left: "53%",
                width: "42%",
                text: "-",
                textAlignment: SMF.UI.TextAlignment.RIGHT,
                fontColor: olderInformation ? '#999999' : "black"
            }));
            lblValue.dataKey = dataKey;
            template.add(lblValue);
            addCounter++;
        }
    }



    var initialSearchbarShift = 40;
    var initialSearchbarShiftMinus = initialSearchbarShift * -1;
    var cntSearchBar = new SMF.UI.Container({
        top: initialSearchbarShiftMinus,
        height: initialSearchbarShift,
        width: "100%",
        left: 0,
        borderWidth: 0
    });
    cntMain.add(cntSearchBar);

    var sd = require("sliderdrawer")(pgUretimlistesi1);



})();
