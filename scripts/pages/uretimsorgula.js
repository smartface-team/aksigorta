/* globals defaultPageAnimation, smfOracle, createLabel, createAwesomeLabel*/
//TODO: include this file in onStart in pages/index.js Use the code below:
//include("pages/acentelistesi.js");
(function() {
    var header = require("header.js");
    var colors = require("colors.js");
    var getUnit = require("getUnit.js");
    var acente = {};
    var baslangicTarihi = null,
        bitisTarihi = null,
        periyot = "";
    var nw = require("nw-smf");
    var Dialog = require("smf-dialog");
    var showServiceError = require("showServiceError");


    var pgUretimSorgula = Pages.pgUretimSorgula = new SMF.UI.Page({
        name: "pgUretimSorgula",
        onKeyPress: pgUretimSorgula_onKeyPress,
        onShow: pgUretimSorgula_onShow,
        backgroundImage: 'stripe.png',
        imageFillType: SMF.UI.ImageFillType.TILE
    });
    module.exports = pgUretimSorgula;

    /**
     * Creates action(s) that are run when the user press the key of the devices.
     * @param {KeyCodeEventArguments} e Uses to for key code argument. It returns e.keyCode parameter.
     * @this Pages.pgUretimSorgula
     */
    function pgUretimSorgula_onKeyPress(e) {
        if (e.keyCode === 4) {
            back();
        }
    }

    /**
     * Creates action(s) that are run when the page is appeared
     * @param {EventArguments} e Returns some attributes about the specified functions
     * @this Pages.acentelistesi
     */
    function pgUretimSorgula_onShow() {
        header.init(this);
        header.setTitle("Üretim Sorgula");
        header.setLeftItem(Device.deviceOS === "iOS" ? "Acenteler" : "Geri", back);


        //We are going w/ dark mode. Our navbar is white.
        SMF.UI.statusBar.style = SMF.UI.StatusBarStyle.DEFAULT;

        smfOracle.logAndFlushAnalytics('pgUretimSorgula_onShow');

    }

    function showData(data) {
        lblBaslangicTarihiData.text = lblBitisTarihiData.text = "(Seç)";
        lblPeriyotData.text = "(YOK)";
        baslangicTarihi = null;
        bitisTarihi = null;
        periyot = "";
        validate();
        acente = data;
        lblAcenteAdi.text = acente.acenteAdi;
        this.show(defaultPageAnimation);
    }
    pgUretimSorgula.showData = showData;

    function back() {
        Pages.back(require("./acentelistesi"));
    }

    function selectBaslangictarihi(e) {
        var maxDate = new Date();
        var currentDate = new Date();
        if (bitisTarihi) {
            maxDate.setDate(1);
            maxDate.setFullYear(Number(String(bitisTarihi).substr(0, 4)));
            maxDate.setMonth(Number(String(bitisTarihi).substr(4, 2)) - 1);
        }
        if (baslangicTarihi) {
            currentDate.setDate(1);
            currentDate.setFullYear(Number(String(baslangicTarihi).substr(0, 4)));
            currentDate.setMonth(Number(String(baslangicTarihi).substr(4, 2)) - 1);
        }
        if (currentDate > maxDate)
            currentDate = maxDate;
        SMF.UI.showDatePicker({
            currentDate: new Date(),
            mask: "YYYY-MM",
            maxDate: maxDate,
            showWorkingDate: true,
            onSelect: function(e) {
                var sDate = new Date(e.date);
                var month = sDate.getMonth() + 1;
                var zeroPad = month < 10 ? "0" : "";
                var year = String(sDate.getFullYear());
                baslangicTarihi = Number(String(year) + zeroPad + String(month));
                lblBaslangicTarihiData.text = month + "." + year;
                validate();
            }
        });
    }

    function selectBitistarihi(e) {
        var minDate = undefined;
        var currentDate = new Date();
        if (baslangicTarihi) {
            minDate = new Date();
            minDate.setDate(1);
            minDate.setFullYear(Number(String(baslangicTarihi).substr(0, 4)));
            minDate.setMonth(Number(String(baslangicTarihi).substr(4, 2)) - 1);
        }
        if (bitisTarihi) {
            currentDate.setDate(1);
            currentDate.setFullYear(Number(String(bitisTarihi).substr(0, 4)));
            currentDate.setMonth(Number(String(bitisTarihi).substr(4, 2)) - 1);
        }
        if (currentDate > minDate)
            currentDate = minDate;
        SMF.UI.showDatePicker({
            currentDate: new Date(),
            mask: "YYYY-MM",
            maxDate: new Date(),
            minDate: minDate,
            showWorkingDate: true,
            onSelect: function(e) {
                var sDate = new Date(e.date);
                var month = sDate.getMonth() + 1;
                var zeroPad = month < 10 ? "0" : "";
                var year = String(sDate.getFullYear());
                bitisTarihi = Number(String(year) + zeroPad + String(month));
                lblBitisTarihiData.text = month + "." + year;
                validate();
            }
        });
    }

    function btmUretimSorgula_onPressed() {
        Dialog.showWait();
        nw.factory("acenteUretimSorgula").body({
            "kanalBilgileri": {
                "kanalId": "1",
                "branchId": "176",
                "token": "_h?xJ_u6?@MCr^z"
            },
            "subeKod": acente.subeKod,
            "kaynakKod": acente.subeKod,
            "acenteNo": acente.acenteKod,
            "sorgulamaBaslamaTarihi": baslangicTarihi,
            "sorgulamaBitisTarihi": bitisTarihi,
            "sorgulamaPeriyot": periyot
        }).result(function(err, data) {
            if (err) {
                alert("Unknown error occured");
            }
            Dialog.removeWait();
            if (Number(data.body.errorCode))
                return showServiceError(data);
            var pgUretimListesi = require("./uretimlistesi");
            var bransUretimleri = data.body.bransUretimListesiType.bransUretimleri;
            var myStrings = ["(YOK)", "Yıllık", "Aylık", "Günlük"];
            var values = ["", "Y", "A", "G"];
            for (var i in bransUretimleri) {
                var bu = bransUretimleri[i];
                switch (bu.gruplamaAdi) {
                    case "KASKO":
                        bu.icon = ""; //shield f132
                        break;
                    case "KONUT":
                        bu.icon = ""; //home f015
                        break;
                    default:
                        bu.icon = ""; //global f0ac
                        // code
                }
                bu.toplamBransKomisyonOrani2 =  String(Math.round(bu.toplamBransKomisyonOrani * 10) / 10).substr(0, 4);
                bu.title = bu.gruplamaAdi[0] +
                    bu.gruplamaAdi.substr(1).toLocaleLowerCase() + " (" +
                    myStrings[values.indexOf(bu.periyot)] + ": " +
                    String(bu.yilAy).substr(4, 2) + "." +
                    String(bu.yilAy).substr(0, 4) + ")";
                bu.field1 = "Prim Tutari: " +
                    bu.toplamBransGerceklesenPrimTutari + " /" +
                    bu.toplamBransHedefPrimTutari + " TL";
                bu.field2 = "Gerçekleşen adet: " + bu.toplamBransGerceklesenAdet;
                bu.field3 = "Komisyon: " + bu.toplamBransGerceklesenKomisyonTutari +
                    "TL (%" + bu.toplamBransKomisyonOrani2 + ")";
                bu.field4 = "Yeni iş poliçe adedi: " + bu.toplamBransYeniIsPoliceAdet;
                bu.field5 = "Yeninelen poliçe adedi: " + bu.toplamBransYenilemePoliceAdet;
                bu.field6 = "Önceki dönem:\n\tPrim tutarı - " + bu.oncekiToplamBransPrimTutari + "\n\tAdet - " + bu.oncekiToplamBransAdet;
                bu.successRate = bu.toplamBransGerceklesenPrimTutari / bu.toplamBransHedefPrimTutari;
                if (bu.successRate > 1)
                    bu.successRate = 1;
                else if (bu.successRate < 0)
                    bu.successRate = 0;
                bu.fieldMega = [bu.field1, bu.field2, bu.field3, bu.field4, bu.field5].join("\n");
                
            }
            console.log("about to show: " + pgUretimListesi.name);
            pgUretimListesi.showData(bransUretimleri);

        })[nw.defaultAction]();
    }

    function selectPeriyot(e) {
        var touchX = Device.touchX;
        var touchY = Device.touchY;
        var myStrings = ["(YOK)", "Yıllık", "Aylık", "Günlük"];
        var values = ["", "Y", "A", "G"];
        var selectedIndexx = values.indexOf(periyot);
        pick(
            myStrings,
            selectedIndexx,
            function(e) {
                lblPeriyotData.text = myStrings[e.index];
                periyot = values[e.index];
                selectedIndexx = e.index;
            },
            function() {},
            function() {},
            touchX,
            touchY);
    }

    function validate() {
        btnUretimSorgula.enabled = !!(baslangicTarihi && bitisTarihi && (baslangicTarihi <= bitisTarihi));
        console.log("btnUretimSorgula.enabled: " + btnUretimSorgula.enabled);
        console.log("baslangicTarihi: "+ baslangicTarihi);
        console.log("bitisTarihi: "+ bitisTarihi);
        btnUretimSorgula.fillColor = btnUretimSorgula.enabled ?
            colors.akSigortaRed : colors.akSigortaRedLightest;
        btnUretimSorgula.fontColor = "white";
    }

    var gapV = 10;
    if (Device.deviceOS === "Android") {
        gapV = (gapV * Device.screenDpi) / 160;
    }


    var lblAcenteAdi = new SMF.UI.Label({
        top: getUnit({
            iOS: 64,
            Android: 80
        }),
        left: 0,
        width: "100%",
        height: "6%",

        fillColor: colors.akSigortaRedLight,
        backgroundTransparent: false,
        fontColor: "white",
        font: new SMF.UI.Font({
            size: Device.deviceOS === "iOS" ? "10pt" : "8pt"
        }),
        textAlignment: SMF.UI.TextAlignment.CENTER,
        roundedEdge: 0
    });
    pgUretimSorgula.add(lblAcenteAdi);
    lblAcenteAdi.bottom = lblAcenteAdi.top + lblAcenteAdi.height;



    // Start Date
    var cntStarts = new SMF.UI.Container({
        name: 'cntStarts',
        left: '4.53%',
        top: lblAcenteAdi.bottom + gapV,
        height: '12%',
        width: '29%',
        borderWidth: 0,
        backgroundTransparent: true
    });

    createLabel(cntStarts, 'lblStart', 'Başlangıç Tarihi', 0, 0, '100%', getUnit({
        iOS: '15%',
        Android: '30%'
    }), SMF.UI.TextAlignment.LEFT, false, Device.deviceOS === "iOS" ? '7pt' : "5pt", false, '#248afd');
    createAwesomeLabel(cntStarts, 'lblDown3', JSON.parse('""'), {iOS:'74%', Android: "76%"}, 0, '50%', getUnit({
        iOS: '15%',
        Android: '30%'
    }), SMF.UI.TextAlignment.LEFT, false, Device.deviceOS === "iOS" ? '7pt' : "5pt", false, '#248afd');

    var lblBaslangicTarihiData = createLabel(cntStarts, 'lblStartDate', '-', 0, '30%', '100%', getUnit({
        iOS: '30%',
        Android: '60%'
    }), SMF.UI.TextAlignment.LEFT, false, "48px" , false, '#4a4a4a', function() {
        selectBaslangictarihi();
    });
    createLabel(cntStarts, 'lblStartTime', '', 0, '70%', getUnit({
        iOS: '92%',
        Android: '85%'
    }), getUnit({
        iOS: '20%',
        Android: '40%'
    }), SMF.UI.TextAlignment.LEFT, false, Device.deviceOS === "iOS" ? '8pt' : "6pt", false, '#4a4a4a', function() {
        selectBaslangictarihi();
    });

    pgUretimSorgula.add(cntStarts);

    // End Date
    var cntEnds = new SMF.UI.Container({
        name: 'cntEnds',
        left: '65%',
        top: lblAcenteAdi.bottom + gapV,
        height: '12%',
        width: '30.6%',
        borderWidth: 0,
        backgroundTransparent: true
    });

    createLabel(cntEnds, 'lblEnd', 'Bitiş Tarihi', 0, 0, '87%', getUnit({
        iOS: '15%',
        Android: '30%'
    }), SMF.UI.TextAlignment.RIGHT, false, Device.deviceOS === "iOS" ? '7pt' : "5pt", false, '#248afd');
    createAwesomeLabel(cntEnds, 'lblDown4', JSON.parse('""'), 0, 0, '97%', getUnit({
        iOS: '15%',
        Android: '30%'
    }), SMF.UI.TextAlignment.RIGHT, false, Device.deviceOS === "iOS" ? '7pt' : "5pt", false, '#248afd');

    var lblBitisTarihiData = createLabel(cntEnds, 'lblEndDate', '-', 0, '30%', '100%', getUnit({
        iOS: '30%',
        Android: '60%'
    }), SMF.UI.TextAlignment.RIGHT, false, "48px", false, '#4a4a4a', function() {
        selectBitistarihi();
    });
    createLabel(cntEnds, 'lblEndTime', '', 0, '70%', '100%', getUnit({
        iOS: '20%',
        Android: '40%'
    }), SMF.UI.TextAlignment.RIGHT, false, Device.deviceOS === "iOS" ? '8pt' : "6pt", false, '#4a4a4a', function() {
        selectBitistarihi();
    });
    pgUretimSorgula.add(cntEnds);
    cntEnds.bottom = cntEnds.top + cntEnds.height;


    var cntPeriyotSec = new SMF.UI.Container({
        borderWidth: 0,
        top: cntEnds.bottom + gapV,
        height: "6%",
        width: "90.94%",
        left: '4.53%'
    });
    pgUretimSorgula.add(cntPeriyotSec);
    cntPeriyotSec.bottom  = cntPeriyotSec.top + cntPeriyotSec.height;

    var lblPeriyot = new SMF.UI.Label({
        top: 0,
        left: 0,
        textAlignment: SMF.UI.TextAlignment.LEFT,
        height: "100%",
        width: "50%",
        text: "Periyot",
        fontColor: "#248afd",
        onTouchEnded: selectPeriyot,
        font: new SMF.UI.Font({
            size: "48px"
        })
    });
    cntPeriyotSec.add(lblPeriyot);


    var lblPeriyotData = new SMF.UI.Label({
        top: 0,
        left: "50%",
        textAlignment: SMF.UI.TextAlignment.RIGHT,
        height: "100%",
        width: "50%",
        text: "(YOK)",
        onTouchEnded: selectPeriyot,
        font: new SMF.UI.Font({
            size: "48px"
        })
    });
    cntPeriyotSec.add(lblPeriyotData);


    var btnUretimSorgula = new SMF.UI.TextButton({
        text: "Üretim Sorgula",
        roundedEdge: 1,
        fillColor: colors.akSigortaRed,
        pressedFillColor: colors.akSigortaRedDark,
        enabled: false,
        top: cntPeriyotSec.bottom + gapV,
        width: "90.94%",
        left: '4.53%',
        height: global.buttonHeight,
        onPressed: btmUretimSorgula_onPressed,
        font: new SMF.UI.Font({
			size: "52px"	
		})
    });
    pgUretimSorgula.add(btnUretimSorgula);



})();
