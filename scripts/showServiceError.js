/* globals */
(function() {
    module.exports = showServiceError;

    function showServiceError(err) {
        if (err.body)
            err = err.body;
        alert({
            title: "Hata (" + err.errorCode + ")",
            message: err.errorMessage

        });
    }
})();
