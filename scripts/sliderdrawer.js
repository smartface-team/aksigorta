/* globals */
(function() {
    var allSliderDrawers = [];
    var header = require("header.js");
    var colors = require("colors.js");
    var getUnit = require("getUnit.js");
    var gap = 30;
    if (Device.deviceOS === "Android")
        gap = (gap * Device.screenDpi) / 160;

    var items = ["Komisyon", "Hasar", "Üretim"];
    var images = ["komisyon.png", "hasar.png", "uretim.png"];
    var imagesAlt = ["komisyon2.png", "hasar2.png", "uretim2.png"];

    function addSliderDrawer(page) {
        if (page.sliderDrawer)
            return page.sliderDrawer;
        var sd = new SMF.UI.SliderDrawer({
            width: "45%",
            touchEnabled: "true",
            backgroundColor: colors.akSigortaRed,
            position: "left" // or can be set as: SMF.UI.SliderDrawerPosition.left
        });
        page.add(sd);
        allSliderDrawers.push(sd);

        var lblTitle = new SMF.UI.Label({
            top: 0,
            left: 0,
            height: getUnit({
                iOS: 112,
                Android: 140
            }),
            width: "100%",
            text: "Performans\nKriterleri",
            multipleLine: true,
            backgroundTransparent: false,
            borderWidth:0,
            fillColor: colors.akSigortaRedDark,
            fontColor: "#FCFAF8",
            font: new SMF.UI.Font({
                size: "72px"
            }),
            textAlignment: SMF.UI.TextAlignment.CENTER
        });
        sd.add(lblTitle);


        var rectSelection = new SMF.UI.Rectangle({
            width: "0.9722222222222222%",
            height: "7,421875%",
            touchEnabled: false,
            roundedEdge: 0,
            borderWidth: 0,
            fillColor: "#FCFAF8",
            left: 0
        });
        sd.add(rectSelection);


        for (var i in items) {
            var btn = new SMF.UI.ImageButton({
                // horizontalGap: gap,
                textAlignment: SMF.UI.TextAlignment.LEFT,
                top: getUnit({
                    iOS: 40 * i + 128,
                    Android: 40 * i + 160
                }),
                left: 0,
                width: "100%",
                text: "              " + items[i],
                height: getUnit(40),
                defaultImage: images[i],
                highlightedImage: imagesAlt[i],
                imageFillType: SMF.UI.ImageFillType.NORMAL,
                positionBackgroundImage: SMF.UI.Alignment.LEFT,
                fontColor: "#FCFAF8",
                pressedFontColor: "#FCFAF8",
                font: new SMF.UI.Font({
                    size: "52px"
                })
            });
            if (items[i] === "Üretim") {
                rectSelection.top = btn.top - gap / 7;
                rectSelection.height = btn.height;
            }
            sd.add(btn);
        }
        return addSliderDrawer;
    }

    module.exports = addSliderDrawer;


})();
